# == Schema Information
#
# Table name: links
#
#  id           :bigint           not null, primary key
#  url          :string
#  slug         :string
#  clicks_count :integer
#  expiry_date  :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  status       :boolean          default(TRUE)
#
# Link class to store and manipulate link records
class Link < ApplicationRecord
  default_scope { where(status: true) }
  scope :recent, -> { where(['expiry_date > ?', 30.days.ago]) }

  has_many :clicks, dependent: :destroy
end
