# == Schema Information
#
# Table name: clicks
#
#  id         :bigint           not null, primary key
#  link_id    :integer
#  ip_address :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Click < ApplicationRecord
  belongs_to :link, counter_cache: true
end
