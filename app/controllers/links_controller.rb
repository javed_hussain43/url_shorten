class LinksController < ApplicationController
  PER_PAGE = 10

  def index
    @links = Link.recent.page(links_params[:page_number]).per(PER_PAGE)
  end

  def detail
    @link = Link.recent.find_by(slug: params[:slug])
  end

  private

  def links_params
    params.permit(:page_number)
  end
end
