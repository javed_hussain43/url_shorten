class CreateClicks < ActiveRecord::Migration[6.0]
  def change
    create_table :clicks do |t|
      t.integer :link_id
      t.string :ip_address
      t.string :country
      t.timestamps
    end
    add_index :clicks, :link_id
  end
end
