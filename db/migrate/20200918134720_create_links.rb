class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :url
      t.string :slug
      t.integer :clicks_count
      t.datetime :expiry_date

      t.timestamps
    end
    add_index :links, :slug, unique: true
  end
end
