class AddStatusToLinks < ActiveRecord::Migration[6.0]
  def change
    add_column :links, :status, :boolean, default: true
    add_index :links, :status
  end
end
