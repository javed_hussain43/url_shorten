# == Route Map
#
#                                Prefix Verb   URI Pattern                                                                              Controller#Action
#                                 links GET    /links(.:format)                                                                         links#index
#                                       POST   /links(.:format)                                                                         links#create
#                              new_link GET    /links/new(.:format)                                                                     links#new
#                                       GET    /:slug(.:format)                                                                         links#show
#
Rails.application.routes.draw do
  resources :links, only: [:new, :create, :index]
  get '/:slug' => 'links#show'
end
